from django import forms
from . models import Registrasi

class RegisForm(forms.ModelForm):
	class Meta:
		model = Registrasi
		fields =[
			'nama_lengkap',
			'email',
			'jenis_kelamin',
			'alamat',
		]

		

		widgets = {
		'nama_lengkap': forms.TextInput(
				attrs ={
					'class':'form-control',
					'placeholder':'Masukan Nama Lengkap Anda',
					}
					),

			  'email': forms.TextInput(
				attrs ={
					'class':'form-control',
					'placeholder':'Isi Dengan Alamat Email Anda',
					}
					),

	  'jenis_kelamin': forms.Select(
				attrs ={
					'class':'form-control',
					},
				choices =[
					('Laki-laki','Laki-laki'),
					('Perempuan','Perempuan'),
					]
				),
	  'alamat'		 : forms.Textarea(
	  		attrs ={
	  			'class':'form-control',
	  		}	)
}