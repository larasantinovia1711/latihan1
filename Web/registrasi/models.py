from django.db import models

# Create your models here.

class Registrasi(models.Model):
	nama_lengkap	= models.CharField(max_length=100)
	email			= models.CharField(max_length=100)
	jenis_kelamin	= models.CharField(max_length=20)
	alamat			= models.CharField(max_length=100)

	def __str__(self):
		return "{}.{}". format(self.id,self.nama_lengkap)