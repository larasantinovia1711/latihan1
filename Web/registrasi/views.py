from django.shortcuts import render, redirect

from . models import Registrasi
from . forms import RegisForm

def delete(request, delete_id):
	Registrasi.objects.filter(id=delete_id).delete()
	return redirect('registrasi:list')

def update(request, update_id):
	akun_update = Registrasi.objects.get(id=update_id)

	data ={
		'nama_lengkap' 		: akun_update.nama_lengkap,
		'email' 			: akun_update.email,
		'jenis_kelamin'		: akun_update.jenis_kelamin,
		'alamat'			: akun_update.alamat,
	}
	akun_form = RegisForm(request.POST or None, initial=data, instance=akun_update)

	if request.method == 'POST':
		if akun_form.is_valid():
			akun_form.save()

		return redirect('registrasi:list')

	context = {
		'page_title':'Update Data',
		'akun_form':akun_form,
	}
	return render(request,'registrasi/create.html', context)

def create(request):
	akun_form = RegisForm(request.POST or None)

	if request.method == 'POST':
		if akun_form.is_valid():
			akun_form.save()

		return redirect('registrasi:list')

	context = {
		'page_title':'Tambah Orang :',
		'akun_form':akun_form,
	}
	return render(request,'registrasi/create.html', context)

def list(request):
	semua_akun = Registrasi.objects.all()
	context = {
		'judul':'Data Pengguna',
		'heading':'Selamat Datang ',
		'subheading':'Di Halamat Registrasi',
		'semua_akun':semua_akun,
	}

	return render(request, 'registrasi/list.html',context)