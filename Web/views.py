from django.shortcuts import render

def index(request):
	context = {
		'judul':'JURNAL PRAKERIN | WEB REGISTRATIONS',
		'heading':'Welcome To My Website',
		'subheading':'Selamat Datang di halaman website saya yang berisi tentang berbagai macam informasi',
	}
	return render(request,'index.html', context)