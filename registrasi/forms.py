from django import forms

from .models import form 

class formForm(forms.ModelForm):
	class Meta:
		model = form 
		fields =[
			'nama',
			'email',
			'jenis_kelamin',
			'alamat',
		]