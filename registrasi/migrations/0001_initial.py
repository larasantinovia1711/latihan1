# -*- coding: utf-8 -*-
# Generated by Django 1.11.17 on 2019-02-09 18:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='form',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=255)),
                ('email', models.CharField(max_length=100)),
                ('jenis_kelamin', models.CharField(max_length=100)),
                ('alamat', models.CharField(max_length=255)),
            ],
        ),
    ]
