# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class form(models.Model):
	nama          = models.CharField(max_length=255)
	email         = models.CharField(max_length=50)
	jenis_kelamin = models.CharField(max_length=50)
	alamat        = models.CharField(max_length=255)

	def __str__(self):
		return "{}.{}".format(self.id,self.email,self.jenis_kelamin,self.alamat)